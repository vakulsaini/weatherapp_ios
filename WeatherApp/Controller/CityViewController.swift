//
//  CityViewController.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class CityViewController: BaseViewController {
        
    @IBOutlet weak var tblView: UITableView!
    
    static let storyboardId = "CityViewController"

    var weatherData: WeatherData?
    
    private var dataSource : CityWeatherDataSource<CityTodayTableViewCell, CityWeatherInfoTableViewCell, CityWeatherForecastCell, DailyForecastData>!
    
    lazy var cityForecastWeatherModel: CityWeatherViewModel = {
       return CityWeatherViewModel()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Setting up the tableView data source for data binding
        setUpDataSource()
        
        // Completion block whenever forecast data changes in viewModel
        cityForecastWeatherModel.bindForecastViewModelToController = {
            self.dataSource.weathers = self.cityForecastWeatherModel.forecastData.daily
            self.reloadTable()
        }
        cityForecastWeatherModel.notifyErrorToController = { error in
            self.showAlert(title: "", message: error.localizedDescription)
        }
        
        if let weatherData = weatherData {
            cityForecastWeatherModel.fetchForecastData(forLocation: weatherData.coord.lat, lng: weatherData.coord.lon)
        }
    }
}

// MARK:- Methods
extension CityViewController {
    
    fileprivate func setUpDataSource() {
        
        self.dataSource = CityWeatherDataSource(todayCellIdentifier: CityTodayTableViewCell.identifier, configureTodayCell: { cell in
            cell.weatherData = self.weatherData
        }, todayWeatherInfoCellIdentifier: CityWeatherInfoTableViewCell.identifier, configureTodayWeatherInfoCell: { cell in
            cell.weatherData = self.weatherData
        }, forecastCellIdentifier: CityWeatherForecastCell.identifier, configureForecastCell: { cell, weatherData in
            cell.dailyWeatherData = weatherData
            if weatherData == nil {
                cell.setAsHeader()
            }
            
        })
        
        self.tblView.dataSource = self.dataSource
    }
    
    fileprivate func reloadTable() {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
}
