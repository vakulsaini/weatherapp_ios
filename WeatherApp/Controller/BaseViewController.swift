//
//  BaseViewController.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class BaseViewController: UIViewController {
    
    private static var mainStoryboard: UIStoryboard = {
        return  UIStoryboard(name: "Main", bundle: nil)
    } ()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}

// MARK:- Methods
extension BaseViewController {
    /// Creates an instance of its child class. The identifier of ViewCotroller in storyboard should be same as name of the class.
    ///
    /// - Returns: an instance of referenced class.
    class func instance() -> UIViewController {
        let className = String(describing: self)
        return BaseViewController.mainStoryboard.instantiateViewController(withIdentifier: className)
    }
}
