//
//  SettingsViewController.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class SettingsViewController: BaseViewController {

    static let storyboardId = "SettingsViewController"
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tempUnit = UserSettingsManager.shared.userSettings.tempUnit
        segment.selectedSegmentIndex = tempUnit == .Celsius ? 0 : 1
    }
}

extension SettingsViewController {
    
    @IBAction func segmentAction(_ segment: UISegmentedControl) {
        let segmentIndex = segment.selectedSegmentIndex
        UserSettingsManager.shared.userSettings.tempUnit = segmentIndex == 0 ? .Celsius : .Fahrenheit
        WPostNotification(name: NotificationsKeys.UnitDidChange)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func resetAction(_ segment: UISegmentedControl) {
        WPostNotification(name: NotificationsKeys.ResetBookmarked)
        dismiss(animated: true, completion: nil)
    }
}
