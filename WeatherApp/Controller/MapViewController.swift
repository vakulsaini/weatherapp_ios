//
//  MapViewController.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit
import MapKit
import CoreLocation

protocol MapViewEventProtocol {
    func mapViewDidCancel()
    func mapViewDidSelectCoordinate(_ coordinate: CLLocationCoordinate2D)
}

enum ZoomLevel: Int {
    case City = 10
    case Street = 15
}

class MapViewController: BaseViewController {
    
    static let storyboardId = "MapViewController"
    
    @IBOutlet weak var mapView: MKMapView!
    
    /// This parameter will hold the center coordinate of map view
    public private (set) var currentCoordinate = CLLocationCoordinate2D()
    
    /// Delegate
    var delegate: MapViewEventProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mapView.delegate = self
        
        // By default map is showing Delhi's location
        // We can replace this by current location as well but for now we're using static coordinates
        let coordinate = CLLocationCoordinate2D(latitude: Delhi.lat, longitude: Delhi.lng)
        self.setZoomLevel(level: .City, forCoordinates: coordinate)
    }
}

// MARK:- Actions
extension MapViewController {
    @IBAction func cancelAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        self.delegate?.mapViewDidCancel()
    }
    
    @IBAction func doneAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
        self.delegate?.mapViewDidSelectCoordinate(currentCoordinate)
    }
}


// MARK:- Methods
extension MapViewController {
    func setZoomLevel(level: ZoomLevel, forCoordinates coordinate: CLLocationCoordinate2D) {
        let mapViewWidth = UIScreen.main.bounds.width // As map view has full width
        let zoomLevel: Double = Double(level.rawValue)
        let span = MKCoordinateSpan(latitudeDelta: 0, longitudeDelta: 360 / pow(2, zoomLevel) * Double(mapViewWidth) / 256)
        self.mapView.setRegion(MKCoordinateRegion(center: coordinate, span: span), animated: true)
    }
}

// MARK:- MapViewDelegates
extension MapViewController: MKMapViewDelegate {
    func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
        currentCoordinate = mapView.centerCoordinate
    }
}
