//
//  HelpViewController.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit
import WebKit

class HelpViewController: BaseViewController {

    static let storyboardId = "HelpViewController"
    
    @IBOutlet weak var webView: WKWebView!
    
    let fileLoderViewModel = HelpViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        fileLoderViewModel.loadFile(webView: webView)
    }
}
