//
//  ViewController.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit
import CoreLocation

class ViewController: BaseViewController {
    
    static let storyboardId = "ViewController"
    
    @IBOutlet weak var tblView: UITableView!
    private var dataSource : WeatherListTableViewDataSource<WeatherListTableViewCell, WeatherData>!
    
    lazy var listWeatherModel: WeatherListViewModel = {
       return WeatherListViewModel()
    }()
    
    lazy var searchBarHandler: WeatherListSearchBarHandler = {
        return WeatherListSearchBarHandler()
    }()
    
    lazy var mapViewController: MapViewController = {
        return MapViewController.instance() as! MapViewController
    }()
    
    lazy var footerView: WeatherListTableViewFooterView = {
        return self.tblView.tableFooterView as! WeatherListTableViewFooterView
    }()
    
    lazy var helpViewController: HelpViewController = {
        return HelpViewController.instance() as! HelpViewController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        // Make some space at bottom of tableView for help floating button
        self.tblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        
        mapViewController.delegate = self
        mapViewController.modalPresentationStyle = .fullScreen
        
        WObserveNotification(name: NotificationsKeys.UnitDidChange, observer: self, selector: #selector(settingsUnitDidUpdate))
        WObserveNotification(name: NotificationsKeys.ResetBookmarked, observer: self, selector: #selector(settingsBookmarkedDidReset))
        
        // Setting up the search bar on navigation bar itself
        setUpSearchBar()
        
        // Setting up the tableView data source for data binding
        setUpDataSource()
        
        // Completion block whenever weathers data changes in viewModel
        listWeatherModel.bindWeatherViewModelToController = {
            self.dataSource.weathers = self.listWeatherModel.weathers
            self.reloadTable()
        }
        
        listWeatherModel.notifyErrorToController = { error in
            self.showAlert(title: "", message: error.localizedDescription)
        }
        
        // Show delhi temperature by default
        self.listWeatherModel.fetchWeatherData(forLocation: Delhi.lat, lng: Delhi.lng)
    }
}


// MARK:- Actions
extension ViewController {
    @IBAction func settingsAction(_ sender: UIButton) {
        let settingsVC = SettingsViewController.instance()
        present(settingsVC, animated: true, completion: nil)
    }
    
    @IBAction func helpAction(_ sender: UIButton) {
        present(helpViewController, animated: true, completion: nil)
    }
}


// MARK:- Methods
extension ViewController {
    
    fileprivate func setUpSearchBar() {
        searchBarHandler.setUpSearchBarOnNavigationItem(navigationItem)
        searchBarHandler.searchBarDidChangeEditing = { text in
            if text.isEmpty {
                self.dataSource.weathers = self.listWeatherModel.weathers
            } else {
                self.dataSource.weathers = self.listWeatherModel.weathers.filter{$0.name.lowercased().contains(text.lowercased())}
            }
            self.reloadTable()
        }
    }
    
    fileprivate func setUpDataSource() {
        
        self.dataSource = WeatherListTableViewDataSource(cellIdentifier: WeatherListTableViewCell.identifier, configureCell: { (cell, weather) in
            cell.backgroundColor = weather.uniqueColor
            cell.lblPlaceName.text = weather.name
            cell.lblTemp.text = UserSettingsManager.shared.getTempInSelectedUnit(temp: weather.main.temp, shouldAppendUnit: false)
        })
        
        // Handling the event for swipe to delete
        self.dataSource.deleteRowAtIndex = { weather in
            // Delete this weather from viewModel list as well
            self.listWeatherModel.deleteWeather(weather)
        }
        
        self.tblView.dataSource = self.dataSource
        self.tblView.delegate = self
        
        // Footer view instance that contains the °C/°F toggle and add more button
        footerView.tempUnit = UserSettingsManager.shared.userSettings.tempUnit
        footerView.addMoreAction = {
            self.openMapView()
        }
        
        footerView.tempUnitDidChange = { segmentIndex in
            UserSettingsManager.shared.userSettings.tempUnit = segmentIndex == 0 ? .Celsius : .Fahrenheit
            self.tblView.reloadData()
        }
    }
    
    fileprivate func reloadTable() {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    fileprivate func openMapView() {
        present(mapViewController, animated: true, completion: nil)
    }
    
    fileprivate func openCityViewForWeather(_ weatherData: WeatherData) {
        let cityViewController = CityViewController.instance() as! CityViewController
        cityViewController.weatherData = weatherData
        present(cityViewController, animated: true, completion: nil)
    }
}


// MARK:- TableView Delegates
extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let weather = listWeatherModel.weathers[indexPath.row]
        // Do your code after clicking on table row
        openCityViewForWeather(weather)
    }
}


// MARK:- MapViewDelegates
extension ViewController: MapViewEventProtocol {
    
    func mapViewDidCancel() {
        debugPrint("Location picking is cancelled...")
    }
    
    func mapViewDidSelectCoordinate(_ coordinate: CLLocationCoordinate2D) {
        debugPrint("Location pickied: \(coordinate)")
        self.listWeatherModel.fetchWeatherData(forLocation: coordinate.latitude, lng: coordinate.longitude)
    }
}


// MARK:- Settings
extension ViewController {
    @objc
    func settingsUnitDidUpdate() {
        footerView.tempUnit = UserSettingsManager.shared.userSettings.tempUnit
        self.tblView.reloadData()
    }
    
    @objc
    func settingsBookmarkedDidReset() {
        listWeatherModel.clear()
    }
}
