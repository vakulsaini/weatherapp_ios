//
//  UIColor+Random.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

extension UIColor {
    static var randomLight: UIColor {
        // If we change alpha to 1, it will create solid colors so changing alpha 0.1 making it light
        return .init(hue: .random(in: 0...1), saturation: 1, brightness: 1, alpha: 0.1)
    }
}
