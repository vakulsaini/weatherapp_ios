//
//  NotificationHelper.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class NotificationHelper: NSObject {

}

struct NotificationsKeys {
    static let UnitDidChange   = "kUnitDidChange"
    static let ResetBookmarked = "kResetBookmarked"
}

func WPostNotification(name: String) {
    NotificationCenter.default.post(name: Notification.Name(name), object: nil, userInfo: nil)
}

func WObserveNotification(name: String, observer: Any, selector: Selector) {
    NotificationCenter.default.addObserver(observer, selector: selector, name: NSNotification.Name(rawValue: name), object: nil)
}
