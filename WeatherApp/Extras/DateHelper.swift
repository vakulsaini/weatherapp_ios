//
//  DateHelper.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class DateHelper: NSObject {
    
    static let shared: DateHelper = DateHelper()
    
    lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        return df
    }()
    
    func getTime(date: Date) -> String {
        dateFormatter.dateFormat = "hh:mm a"
        return dateFormatter.string(from: date)
    }
    
    func getDay(date: Date) -> String {
        dateFormatter.dateFormat = "EEE"
        return dateFormatter.string(from: date)
    }
}
