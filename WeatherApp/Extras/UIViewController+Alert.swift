//
//  UIViewController+Alert.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

extension UIViewController {
    func showAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        DispatchQueue.main.async {
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            self.present(alert, animated: true) { }
        }
    }
}
