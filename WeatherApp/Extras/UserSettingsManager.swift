//
//  UserSettingsManager.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

class UserSettingsManager: NSObject {
    static let shared: UserSettingsManager = UserSettingsManager()
    var userSettings = UserSettings()
    
    func getTempInSelectedUnit(temp: Double, shouldAppendUnit: Bool) -> String {
        var _temp = temp
        if self.userSettings.tempUnit == .Fahrenheit {
            // As we request the data in celsius only
            // Need to convert it into fahrenheit
            _temp = UserSettingsManager.celsiusToFahrenheit(temp)
        }
        
        if shouldAppendUnit {
            return "\(Int(round(_temp)))" + self.userSettings.tempUnit.unitString
        }
        else {
            return "\(Int(round(_temp)))°"
        }
    }
}

extension UserSettingsManager {
    static func celsiusToFahrenheit(_ celsius: Double) -> Double {
        return (celsius * 9.0/5.0) + 32.0
    }
    static func fahrenheitToCelsius(_ fahrenheit: Double) -> Double {
        return (fahrenheit - 32.0) * 5.0/9.0
    }
}
