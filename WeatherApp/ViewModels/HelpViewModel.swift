//
//  HelpViewModel.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit
import WebKit

class HelpViewModel: NSObject {
    private var fileURL: URL? {
        return Bundle.main.url(forResource: "help", withExtension: "html")
    }
    
    func loadFile(webView: WKWebView) {
        guard let url = fileURL else {
            return
        }
        webView.loadFileURL(url, allowingReadAccessTo: url)
    }
}
