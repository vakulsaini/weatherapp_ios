//
//  CityWeatherDataSource.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class CityWeatherDataSource<TodayCell: UITableViewCell, TodayWetherInfoCell: UITableViewCell, ForecastCell: UITableViewCell, T>: NSObject, UITableViewDataSource {
    
    private var todayCellIdentifier: String!
    private var todayWeatherInfoCellIdentifier: String!
    private var forecastCellIdentifier: String!
    
    /// Contains the today's weather data such as place name, tempreature etc
    var configureTodayCell: (TodayCell) -> Void = {_ in}
    
    /// Contains today's weather information such as wind/pressure/humidity etc.
    var configureTodayWeatherInfoCell: (TodayWetherInfoCell) -> Void = {_ in}
    
    /// Contains next 5 days weather forecast data
    var weathers: [T] = []
    var configureForecastCell: (ForecastCell, T?) -> Void = {_,_ in}
    
    
    init(todayCellIdentifier : String, configureTodayCell : @escaping (TodayCell) -> Void,
         todayWeatherInfoCellIdentifier : String, configureTodayWeatherInfoCell : @escaping (TodayWetherInfoCell) -> Void,
         forecastCellIdentifier: String, configureForecastCell: @escaping (ForecastCell, T?) -> Void) {
        
        self.todayCellIdentifier = todayCellIdentifier
        self.configureTodayCell = configureTodayCell
        
        self.todayWeatherInfoCellIdentifier = todayWeatherInfoCellIdentifier
        self.configureTodayWeatherInfoCell = configureTodayWeatherInfoCell
        
        self.forecastCellIdentifier = forecastCellIdentifier
        self.configureForecastCell = configureForecastCell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // First section for today's weather info and second section will show the next 5 days weather forecast
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            // First row is for place name and temperture
            // Second row is for weather information such as wind/pressure/humidity etc.
            return 2
        }
        
        // 1 extra cell for header to showing the Labels, TEMP, SPEED, RAIN at top
        return weathers.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: todayCellIdentifier, for: indexPath) as! TodayCell
                configureTodayCell(cell)
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: todayWeatherInfoCellIdentifier, for: indexPath) as! TodayWetherInfoCell
                configureTodayWeatherInfoCell(cell)
                return cell
            }
        }
        else {
            // Next 5 days Forecast
            let cell = tableView.dequeueReusableCell(withIdentifier: forecastCellIdentifier, for: indexPath) as! ForecastCell
            if indexPath.row == 0  {
                // This is for header cell
                configureForecastCell(cell, nil)
            }
            else {
                let weatherData = weathers[indexPath.row - 1]
                configureForecastCell(cell, weatherData)
            }
            return cell
        }
    }
}
