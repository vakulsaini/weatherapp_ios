//
//  WeatherListTableViewDataSource.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

class WeatherListTableViewDataSource<Cell: UITableViewCell, T>: NSObject, UITableViewDataSource {
    
    private var cellIdentifier: String!
    var weathers: [T] = []
    var configureCell: (Cell, T) -> Void = {_,_ in}
    var deleteRowAtIndex: (T) -> Void = { _ in}
    
    init(cellIdentifier : String, configureCell : @escaping (Cell, T) -> Void) {
        self.cellIdentifier = cellIdentifier
        self.configureCell = configureCell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! Cell
        let weather = weathers[indexPath.row]
        configureCell(cell, weather)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let weather = weathers.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            deleteRowAtIndex(weather)
        }
    }
}
