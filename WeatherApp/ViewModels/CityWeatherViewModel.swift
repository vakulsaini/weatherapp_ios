//
//  CityWeatherViewModel.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class CityWeatherViewModel: NSObject {
    private lazy var apiManager : APIManager = {
        return APIManager()
    }()
    
    /*private(set) var forecastData : ForecastData = ForecastData(cod: "", list: []) {
        didSet {
            // Notify the controller
            self.bindForecastViewModelToController()
        }
    }*/
    
    private(set) var forecastData : WeeklyForecast = WeeklyForecast(lat: 0, lon: 0, daily: []) {
        didSet {
            // Notify the controller
            self.bindForecastViewModelToController()
        }
    }

    var bindForecastViewModelToController : (() -> ()) = {}
    var notifyErrorToController : ((Error) -> ()) = {_ in}

    func fetchForecastData(forLocation lat: Double, lng: Double) {
        /*apiManager.getWeatherForecastData(forLocation: lat, lng: lng) { forecastData in
            if let forecastData = forecastData {
                self.forecastData = forecastData
            }
        }*/
        
        apiManager.getWeeklyWeatherForecastData(forLocation: lat, lng: lng) { weeklyForecast, error in
            
            if let error = error {
                self.notifyErrorToController(error)
                return
            }
            
            if let forecastData = weeklyForecast {
                self.forecastData = forecastData
            }
        }
    }
}
