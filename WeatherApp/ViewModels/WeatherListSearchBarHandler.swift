//
//  WeatherListSearchBarHandler.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class WeatherListSearchBarHandler: NSObject, UISearchResultsUpdating {
    
    var searchBarDidChangeEditing: (String) -> Void = {_ in}
    
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text {
            // Filter by the user input
            searchBarDidChangeEditing(text)
        }
        else {
            // Clear the filter because search bar is empty
            searchBarDidChangeEditing("")
        }
    }
    
    func setUpSearchBarOnNavigationItem(_ navigationItem: UINavigationItem) {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.searchController = searchController
    }
}
