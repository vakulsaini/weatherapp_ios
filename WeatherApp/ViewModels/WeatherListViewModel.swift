//
//  WeatherListViewModel.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

class WeatherListViewModel: NSObject {
    
    private lazy var apiManager : APIManager = {
        return APIManager()
    }()
    
    private(set) var weathers : [WeatherData] = []
    
    var bindWeatherViewModelToController : (() -> ()) = {}
    var notifyErrorToController : ((Error) -> ()) = {_ in}

    func fetchWeatherData(forLocation lat: Double, lng: Double) {
        apiManager.getWeatherData(forLocation: lat, lng: lng) { weatherData, error in
            
            if let error = error {
                self.notifyErrorToController(error)
                return
            }
            
            if let weatherData = weatherData {
                // Append this new weather data into array
                self.weathers.append(weatherData)
            
                // Notify the controller
                self.bindWeatherViewModelToController()
            }
        }
    }
    
    func deleteWeather(_ weatherData: WeatherData) {
        weathers.removeAll {$0.id == weatherData.id}
    }
    
    func clear() {
        weathers.removeAll()
        bindWeatherViewModelToController()
    }
}
