//
//  ForecastData.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import Foundation
import UIKit

struct ForecastData: Decodable {
    let cod: String
    let list: [WeatherData]
}
