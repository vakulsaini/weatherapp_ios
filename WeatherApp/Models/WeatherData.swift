//
//  Weather.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import Foundation
import UIKit

// chance of rain

// sunrise, sunset, wind, temp max, temp min, humidity, pressure

struct WeatherData : Decodable {
    let coord : Coordinate
    let id, timezone : Int
    let name : String
    let weather : [Weather]
    let sys : Sys
    let main : Main
    let wind : Wind
    let dt : Date
    var uniqueColor: UIColor?
    
    enum CodingKeys: String, CodingKey {
        case coord
        case id
        case timezone
        case name
        case weather
        case sys
        case main
        case wind
        case dt
    }
    
    init (from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if values.contains(.coord) {
            self.coord = try values.decode(Coordinate.self, forKey: .coord)
        } else {
            self.coord = Coordinate(lat: 0, lon: 0)
        }
        
        if values.contains(.id) {
            self.id = try values.decode(Int.self, forKey: .id)
        } else {
            self.id = 0
        }
        
        if values.contains(.timezone) {
            self.timezone = try values.decode(Int.self, forKey: .timezone)
        } else {
            self.timezone = 0
        }

        if values.contains(.name) {
            self.name = try values.decode(String.self, forKey: .name)
        } else {
            self.name = ""
        }
        
        self.weather = try values.decode([Weather].self, forKey: .weather)
        self.sys = try values.decode(Sys.self, forKey: .sys)
        self.main = try values.decode(Main.self, forKey: .main)
        self.wind = try values.decode(Wind.self, forKey: .wind)
        self.dt = try values.decode(Date.self, forKey: .dt)
    }
}

struct Coordinate : Decodable {
    let lat, lon : Double
}

struct Weather : Decodable {
    let id : Int
    let icon : String
    let main : String
    let description: String
}

struct Sys : Decodable {
    let sunrise, sunset : Date?
    let country : String
    
    enum CodingKeys: String, CodingKey {
        case sunrise
        case sunset
        case country
    }
    
    init (from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        if values.contains(.sunrise) {
            self.sunrise = try values.decode(Date.self, forKey: .sunrise)
        } else {
            self.sunrise = nil
        }
        
        if values.contains(.sunset) {
            self.sunset = try values.decode(Date.self, forKey: .sunset)
        } else {
            self.sunset = nil
        }
        
        if values.contains(.country) {
            self.country = try values.decode(String.self, forKey: .country)
        } else {
            self.country = ""
        }
    }
}

struct Main : Decodable {
    let temp, tempMin, tempMax : Double
    let pressure, humidity : Int
}

struct Wind : Decodable {
    let speed : Double
}

