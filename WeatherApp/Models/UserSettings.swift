//
//  UserSettings.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

struct UserSettings {
    
    /// Default is celsius
    var tempUnit: TempUnit = .Celsius
}

enum TempUnit {
    case Celsius
    case Fahrenheit
    
    var unitString: String {
        return self == .Celsius ? "°C" : "°F"
    }
}
