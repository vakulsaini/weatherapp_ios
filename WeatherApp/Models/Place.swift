//
//  Place.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import Foundation

struct Place {
    let lat, lng : Double
    let name: String
    init(name: String, lat: Double, lng: Double) {
        self.name = name
        self.lat = lat
        self.lng = lng
    }
}
