//
//  WeeklyForecast.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import Foundation
import UIKit

struct WeeklyForecast: Decodable {
    let lat: Double
    let lon: Double
    let daily: [DailyForecastData]
}

struct DailyForecastData: Decodable {
    let dt: Date
    let sunrise: Date
    let sunset: Date
    let moonrise: Date
    let moonset: Date
    let moonPhase: Double
    let pressure: Int
    let humidity: Int
    let dewPoint: Double
    let windSpeed: Double
    let windDeg: Int
    let windGust: Double
    let clouds: Int
    let pop: Double
    let rain: Double
    let uvi: Double
    
    let temp: Temp
    let feelsLike: FeelsLike
    let weather: [Weather]
    
    
    enum CodingKeys: String, CodingKey {
        case dt
        case sunrise
        case sunset
        case moonrise
        case moonset
        case moonPhase
        case pressure
        case humidity
        case dewPoint
        case windSpeed
        case windDeg
        case windGust
        case clouds
        case pop
        case rain
        case uvi
        
        case temp
        case feelsLike
        case weather
    }
    
    init (from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if values.contains(.rain) {
            self.rain = try values.decode(Double.self, forKey: .rain)
        } else {
            self.rain = 0
        }
        
        self.dt = try values.decode(Date.self, forKey: .dt)
        self.sunrise = try values.decode(Date.self, forKey: .sunrise)
        self.sunset = try values.decode(Date.self, forKey: .sunset)
        self.moonrise = try values.decode(Date.self, forKey: .moonrise)
        self.moonset = try values.decode(Date.self, forKey: .moonset)
        self.moonPhase = try values.decode(Double.self, forKey: .moonPhase)
        
        self.pressure = try values.decode(Int.self, forKey: .pressure)
        self.humidity = try values.decode(Int.self, forKey: .humidity)
        self.dewPoint = try values.decode(Double.self, forKey: .dewPoint)
        self.windSpeed = try values.decode(Double.self, forKey: .windSpeed)
        self.windDeg = try values.decode(Int.self, forKey: .windDeg)
        self.windGust = try values.decode(Double.self, forKey: .windGust)
        
        self.clouds = try values.decode(Int.self, forKey: .clouds)
        self.pop = try values.decode(Double.self, forKey: .pop)
        self.uvi = try values.decode(Double.self, forKey: .uvi)
        
        self.temp = try values.decode(Temp.self, forKey: .temp)
        self.feelsLike = try values.decode(FeelsLike.self, forKey: .feelsLike)
        self.weather = try values.decode([Weather].self, forKey: .weather)
    }
}

struct Temp: Decodable {
    let day: Double
    let min: Double
    let max: Double
    let night: Double
    let eve: Double
    let morn: Double
}

struct FeelsLike: Decodable {
    let day: Double
    let night: Double
    let eve: Double
    let morn: Double
}


