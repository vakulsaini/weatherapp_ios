//
//  CityTodayTableViewCell.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class CityTodayTableViewCell: UITableViewCell {

    static let identifier = "CityTodayTableViewCell"
    
    @IBOutlet weak var viewInfoContainer: UIView!
    @IBOutlet weak var lblPlaceName: UILabel!
    @IBOutlet weak var lblWeatherDesc: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    
    var weatherData: WeatherData? {
        didSet {
            loadWeatherData(weatherData)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    fileprivate func clearValues() {
        lblPlaceName.text = "--"
        lblWeatherDesc.text = "--"
        lblTemp.text = UserSettingsManager.shared.userSettings.tempUnit.unitString
        viewInfoContainer.backgroundColor = .systemGray6
    }
    
    func loadWeatherData(_ weatherData: WeatherData?) {
        clearValues()
        guard let weatherData = weatherData else {
            return
        }
        
        viewInfoContainer.backgroundColor = weatherData.uniqueColor
        lblPlaceName.text = weatherData.name.uppercased()
        if let weather = weatherData.weather.first {
            lblWeatherDesc.text = "\(weather.main), \(weather.description)"
        }
        lblTemp.text = UserSettingsManager.shared.getTempInSelectedUnit(temp: weatherData.main.temp, shouldAppendUnit: true)
    }
}
