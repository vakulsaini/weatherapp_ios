//
//  CityWeatherInfoTableViewCell.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class CityWeatherInfoTableViewCell: UITableViewCell {

    static let identifier = "CityWeatherInfoTableViewCell"
    
    @IBOutlet weak var lblSunrise: UILabel!
    @IBOutlet weak var lblSunset: UILabel!
    @IBOutlet weak var lblTempMax: UILabel!
    @IBOutlet weak var lblTempMin: UILabel!
    @IBOutlet weak var lblWind: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    @IBOutlet weak var lblRain: UILabel!
    
    var weatherData: WeatherData? {
        didSet {
            loadWeatherData(weatherData)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    fileprivate func clearValues() {
        lblSunrise.text = "--"
        lblSunset.text = "--"
        lblTempMax.text = UserSettingsManager.shared.userSettings.tempUnit.unitString
        lblTempMin.text = UserSettingsManager.shared.userSettings.tempUnit.unitString
        lblWind.text = "-- kph"
        lblHumidity.text = "-- %"
        lblPressure.text = "__ hpa"
        lblRain.text = "__ %"
    }
    
    func loadWeatherData(_ weatherData: WeatherData?) {
        clearValues()
        guard let weatherData = weatherData else {
            return
        }
        
        if let sunrise = weatherData.sys.sunrise {
            lblSunrise.text = DateHelper.shared.getTime(date: sunrise)
        }
        if let sunset = weatherData.sys.sunset {
            lblSunset.text = DateHelper.shared.getTime(date: sunset)
        }
        
        lblTempMin.text = UserSettingsManager.shared.getTempInSelectedUnit(temp: weatherData.main.tempMin, shouldAppendUnit: true)
        lblTempMax.text = UserSettingsManager.shared.getTempInSelectedUnit(temp: weatherData.main.tempMax, shouldAppendUnit: true)
        
        lblWind.text = "\(weatherData.wind.speed) kph"
        lblHumidity.text = "\(weatherData.main.humidity) %"
        lblPressure.text = "\(weatherData.main.pressure) hpa"
    }
}
