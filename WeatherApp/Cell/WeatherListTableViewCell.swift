//
//  WeatherListTableViewCell.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

class WeatherListTableViewCell: UITableViewCell {

    static let identifier = "WeatherListTableViewCell"
    
    @IBOutlet weak var lblPlaceName: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
