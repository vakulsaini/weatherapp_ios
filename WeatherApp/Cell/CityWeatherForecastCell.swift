//
//  CityWeatherForecastCell.swift
//  WeatherApp
//
//  Created by Vakul Saini on 13/06/21.
//

import UIKit

class CityWeatherForecastCell: UITableViewCell {

    static let identifier = "CityWeatherForecastCell"
    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    @IBOutlet weak var lblWind: UILabel!
    @IBOutlet weak var lblRain: UILabel!
    
    /*var weatherData: WeatherData? {
        didSet {
            loadWeatherData(weatherData)
        }
    }*/
    
    var dailyWeatherData: DailyForecastData? {
        didSet {
            loadWeatherData(dailyWeatherData)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    fileprivate func clearValues() {
        lblDay.text = ""
        lblTemp.text = UserSettingsManager.shared.userSettings.tempUnit.unitString
        lblWind.text = "-- kph"
        lblRain.text = "__ %"
    }
    
    /*func loadWeatherData(_ weatherData: WeatherData?) {
        clearValues()
        guard let weatherData = weatherData else {
            return
        }
        
        lblTemp.text = UserSettingsManager.shared.getTempInSelectedUnit(temp: weatherData.main.temp, shouldAppendUnit: true)
        lblWind.text = "\(weatherData.wind.speed) kph"
        //lblRain.text = "__ %"
    }*/
    
    func loadWeatherData(_ weatherData: DailyForecastData?) {
        clearValues()
        guard let weatherData = weatherData else {
            return
        }
        
        lblTemp.text = UserSettingsManager.shared.getTempInSelectedUnit(temp: weatherData.temp.day, shouldAppendUnit: true)
        lblWind.text = "\(Int(round(weatherData.windSpeed))) kph"
        lblRain.text = "\(Int(round(weatherData.rain))) %"
        lblDay.text  = DateHelper.shared.getDay(date: weatherData.dt)
    }
    
    func setAsHeader() {
        lblDay.text = ""
        lblTemp.text = "TEMP"
        lblWind.text = "WIND"
        lblRain.text = "RAIN"
    }
}
