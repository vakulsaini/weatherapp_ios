//
//  WeatherListTableViewFooterView.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

class WeatherListTableViewFooterView: UIView {
    
    @IBOutlet weak var segment: UISegmentedControl!
    @IBOutlet weak var btnAddMore: UIButton!
    
    var tempUnitDidChange: ((Int) -> ()) = { _ in }
    var addMoreAction: (() -> ()) = { }
    
    var tempUnit: TempUnit = .Celsius {
        didSet {
            segment.selectedSegmentIndex = tempUnit == .Celsius ? 0 : 1
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnAddMore.layer.cornerRadius = btnAddMore.frame.width / 2
        btnAddMore.layer.borderWidth  = 1
        btnAddMore.layer.borderColor  = UIColor.label.cgColor
    }
}

// MARK:- Actions
extension WeatherListTableViewFooterView {
    @IBAction func addAction(_ sender: UIButton) {
        addMoreAction()
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        tempUnitDidChange(sender.selectedSegmentIndex)
    }
}

