//
//  APIManager.swift
//  WeatherApp
//
//  Created by Vakul Saini on 12/06/21.
//

import UIKit

let Delhi = Place(name: "Delhi", lat: 28.7041, lng: 77.1025)

class APIManager: NSObject {
    var session: URLSession = URLSession.shared
    var currentTask: URLSessionDataTask?
    
    static var WEATHER_API_KEY = ""
    static var BASE_URL = ""
}

// MARK:- Single day data API code
extension APIManager {
    
    private func weatherURL(forLocation lat: Double, lng: Double) -> URL? {
        return URL(string: "\(APIManager.BASE_URL)/weather?lat=\(lat)&lon=\(lng)&appid=\(APIManager.WEATHER_API_KEY)&units=metric")
    }
    
    func getWeatherData(forLocation lat: Double, lng: Double, completion : @escaping (WeatherData?, Error?) -> ()){
        
        if lat == 0 || lng == 0 {
            let error = NSError.errorWithMessage(msg: "Latitude or longitude can not be empty.")
            completion(nil, error)
            return
        }
        
        guard let url = weatherURL(forLocation: lat, lng: lng) else {
            let error = NSError.errorWithMessage(msg: "Invalid URL.")
            completion(nil, error)
            return
        }
        
        currentTask = session.dataTask(with: url) { (data, urlResponse, error) in
            
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let data = data else {
                let error = NSError.errorWithMessage(msg: "Empty data.")
                completion(nil, error)
                return
            }
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = .secondsSince1970
                var weatherData = try decoder.decode(WeatherData.self, from: data)
                weatherData.uniqueColor = UIColor.randomLight
                completion(weatherData, nil)

            } catch {
                print("Error serializing Json: ", error)
                completion(nil, error)
            }
            
        }
        currentTask?.resume()
    }
}


// MARK:- Weekly weather data API Code
extension APIManager {
    private func weatherWeeklyForecastURL(forLocation lat: Double, lng: Double) -> URL? {
        return URL(string: "\(APIManager.BASE_URL)/onecall?lat=\(lat)&lon=\(lng)&exclude=current,minutely,hourly,alerts&appid=\(APIManager.WEATHER_API_KEY)&units=metric")
    }
    
    func getWeeklyWeatherForecastData(forLocation lat: Double, lng: Double, completion : @escaping (WeeklyForecast?, Error?) -> ()){
        
        if lat == 0 || lng == 0 {
            let error = NSError.errorWithMessage(msg: "Latitude or longitude can not be empty.")
            completion(nil, error)
            return
        }
        
        guard let url = weatherWeeklyForecastURL(forLocation: lat, lng: lng) else {
            let error = NSError.errorWithMessage(msg: "Invalid URL.")
            completion(nil, error)
            return
        }
        
        currentTask = session.dataTask(with: url) { (data, urlResponse, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            
            guard let data = data else {
                let error = NSError.errorWithMessage(msg: "Empty data.")
                completion(nil, error)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                decoder.dateDecodingStrategy = .secondsSince1970
                let forecastData = try decoder.decode(WeeklyForecast.self, from: data)
                completion(forecastData, nil)

            } catch {
                print("Error serializing Json: ", error)
                completion(nil, error)
            }
            
        }
        currentTask?.resume()
    }
}


extension NSError {
    static func errorWithMessage(msg: String) -> NSError {
        return NSError(domain: "app.error", code: 1234, userInfo: [NSLocalizedDescriptionKey: msg])
    }
}


/*private func weatherForecastURL(forLocation lat: Double, lng: Double) -> URL? {
    if lat == 0 || lng == 0 {
        return nil
    }
    return URL(string: "\(BASE_URL)/forecast?lat=\(lat)&lon=\(lng)&appid=\(WEATHER_API_KEY)&units=metric")
}

func getWeatherForecastData(forLocation lat: Double, lng: Double, completion : @escaping (ForecastData?) -> ()){
    guard let url = weatherForecastURL(forLocation: lat, lng: lng) else {
        completion(nil)
        return
    }
    
    let task = URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
        guard let data = data else {
            completion(nil)
            return
        }
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            decoder.dateDecodingStrategy = .secondsSince1970
            let forecastData = try decoder.decode(ForecastData.self, from: data)
            completion(forecastData)

        } catch {
            print("Error serializing Json: ", error)
            completion(nil)
        }
        
    }
    task.resume()
}*/
