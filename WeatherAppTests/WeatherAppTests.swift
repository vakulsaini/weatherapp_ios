//
//  WeatherAppTests.swift
//  WeatherAppTests
//
//  Created by Vakul Saini on 12/06/21.
//

import XCTest
@testable import WeatherApp

class WeatherAppTests: XCTestCase {

    let delhi = Place(name: "Delhi", lat: 28.7041, lng: 77.1025)
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    // ------- Testing for single day weather data ------- //
    func testExpectedURLHostAndPath() {
        let apiManager = APIManager()
        apiManager.getWeatherData(forLocation: delhi.lat, lng: delhi.lng) { weatherData, error in
            
        }
        XCTAssertEqual(apiManager.currentTask?.currentRequest?.url?.host, "api.openweathermap.org")
        XCTAssertEqual(apiManager.currentTask?.currentRequest?.url?.path, "/data/2.5/weather")
    }
    
    func testGetWeatherDataSuccessfully() {
        let apiManager = APIManager()
        let weatherDataExpectation = expectation(description: "weatherData")
        var response: WeatherData?
        apiManager.getWeatherData(forLocation: delhi.lat, lng: delhi.lng) { weatherData, error in
            response = weatherData
            weatherDataExpectation.fulfill()
        }
        waitForExpectations(timeout: 2) { (error) in
            XCTAssertNotNil(response)
        }
    }
    
    func testGetWeatherDataReturnsError() {
        let apiManager = APIManager()
        let errorExpectation = expectation(description: "error")
        var errorResponse: Error?
        apiManager.getWeatherData(forLocation: 0, lng: 0) { weatherData, error in
            errorResponse = error
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
    
    
    // ------- Testing for weekly weather data ------- //
    func testExpectedURLHostAndPathForWeeklyWeatherData() {
        let apiManager = APIManager()
        apiManager.getWeeklyWeatherForecastData(forLocation: delhi.lat, lng: delhi.lng) { weeklyForecast, error in
            
        }
        XCTAssertEqual(apiManager.currentTask?.currentRequest?.url?.host, "api.openweathermap.org")
        XCTAssertEqual(apiManager.currentTask?.currentRequest?.url?.path, "/data/2.5/onecall")
    }
    
    
    func testGetWeeklyWeatherDataSuccessfully() {
        let apiManager = APIManager()
        let weatherDataExpectation = expectation(description: "weeklyWeatherData")
        var response: WeeklyForecast?
        
        apiManager.getWeeklyWeatherForecastData(forLocation: delhi.lat, lng: delhi.lng) { weeklyForecast, error in
            response = weeklyForecast
            weatherDataExpectation.fulfill()
        }
        waitForExpectations(timeout: 2) { (error) in
            XCTAssertNotNil(response)
        }
    }
    
    func testGetWeeklyWeatherDataReturnsError() {
        let apiManager = APIManager()
        let errorExpectation = expectation(description: "error")
        var errorResponse: Error?
        apiManager.getWeeklyWeatherForecastData(forLocation: 0, lng: 0) { weatherData, error in
            errorResponse = error
            errorExpectation.fulfill()
        }
        waitForExpectations(timeout: 1) { (error) in
            XCTAssertNotNil(errorResponse)
        }
    }
}
